import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todo_list_firebase_internet/model/todo.dart';

class DataBaseService{
  CollectionReference todoCollections = FirebaseFirestore.instance.collection("Todos");

  Future createNewtodo(String title) async{
    return await todoCollections.add({
      "title": title,
      "isComplet": false,
    });
  }
  Future completTask(uid) async {
     await todoCollections.doc(uid).update({"isComplet": true});
  }

  Future removeTodo(uid)async{
    await todoCollections.doc(uid).delete();
  }

  List<Todo>? todoFromFirestore(QuerySnapshot snapshot) {
    if (snapshot != null) {
      return snapshot.docs.map((e) {
        return Todo(
          iscomplet: (e.data()as dynamic)["isComplet"],
          title: (e.data()as dynamic)["title"],
          uid: e.id,
        );
      }).toList();
    } else {
      return null;
    }
  }
    Stream<List<Todo>?> listTodos(){
   return todoCollections.snapshots().map((todoFromFirestore));

  }
}