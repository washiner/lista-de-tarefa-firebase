import 'package:flutter/material.dart';
import 'package:todo_list_firebase_internet/loading.dart';
import 'package:todo_list_firebase_internet/model/todo.dart';
import 'package:todo_list_firebase_internet/services/database_services.dart';

class TodoList extends StatefulWidget {
  @override
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  TextEditingController todoTitleController = TextEditingController();
  bool isComplet = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: SafeArea(
              child: StreamBuilder<List<Todo>?>(
                stream: DataBaseService().listTodos(),
                builder: (context, snapshot) {
                  if(!snapshot.hasData){
                    return Loading();
                  }
                  List<Todo>? todos = snapshot.data;
                  return Padding(
            padding: EdgeInsets.all(25),
            child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "LISTA DE TAREFAS",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Divider(),
                    SizedBox(height: 30),
                    ListView.separated(
                      separatorBuilder: (context, index)=> Divider(thickness: 2 ,color: Colors.grey[800],),
                      shrinkWrap: true,
                      itemCount: todos!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Dismissible(
                          key: Key(todos![index].title),
                          background: Container(
                            padding: EdgeInsets.only(left: 20),
                            alignment: Alignment.centerLeft,
                            child: Icon(Icons.delete),
                            color: Colors.red,
                          ),
                          onDismissed: (direction)async{
                            await DataBaseService().removeTodo(todos[index].uid);

                          },
                          child: ListTile(
                            onTap: () {
                             DataBaseService().completTask(todos[index].uid);
                            },
                            leading: Container(
                              padding: EdgeInsets.all(2),
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Theme.of(context).primaryColor,
                              ),
                              child: todos[index].iscomplet ? Icon(
                                Icons.check,
                                color: Colors.white,
                              ) : Container(),
                            ),
                            title: Text(
                              todos[index].title,
                              style: TextStyle(
                                color: Colors.grey[200],
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  ],
            ),
          );
                }
              )),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Theme.of(context).primaryColor,
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                    backgroundColor: Colors.grey[800],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    content: Container(
                      height: 250,
                      width: 300,
                      child: Column(
                        children: [
                          Text(
                            "Adicionar tarefa",
                            style: TextStyle(fontSize: 20, color: Colors.white),
                          ),
                          SizedBox(height: 30),
                          TextFormField(
                            controller: todoTitleController,
                            style: TextStyle(
                                fontSize: 18, height: 2, color: Colors.white),
                            decoration: InputDecoration(
                                hintText: "Digite sua nova tarefa",
                                hintStyle: TextStyle(color: Colors.white),
                               ),
                            autofocus: true,
                          ),
                          SizedBox(height: 30),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ElevatedButton(
                                child: Text("Adicionar"),
                                onPressed: () async {
                                  if(todoTitleController.text.isNotEmpty){
                                  await DataBaseService().createNewtodo(todoTitleController.text.trim());

                                    Navigator.of(context).pop();
                                    todoTitleController.text = "";
                                  }
                                },

                              ),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text("Cancelar"),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                });
          },
        ));
  }
}
